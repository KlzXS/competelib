#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[]){
	if(argc < 2){
		fprintf(stderr, "No file supplied! Usage: %s [in_file].\n", argv[0]);
		return -1;
	}

	int flags = 0;
	{int i;
	for(i = 2; i < argc; ++i){
		if(!strcmp("-s", argv[i])) flags |= (1<<0);
	}};

	FILE* filein = fopen(argv[1], "r");
	if(!filein){
		perror("ERROR");
		return -1;
	}

	char* out_name;
	{ int len = strlen(argv[1]);
	int cur_pos = len-1;
	while(argv[1][cur_pos--] != '.');
	out_name = (char*)calloc(cur_pos+2, 1);
	memcpy(out_name, argv[1], cur_pos+1); };

	FILE* fileout;
	if(flags&(1<<0)) fileout = stdout;
	else fileout = fopen(out_name, "w");
	if(!fileout){
		perror("ERROR");
		return -1;
	}

	int c; int pos = 0;
	char* buffer = (char*)calloc(32*1024, 1);
	while((c = fgetc(filein)) != EOF){ buffer[pos++] = c; }

	free(out_name);
	fclose(filein);

	char* begin = NULL;
	char* end = NULL;

	const char* begin_str = "<![CDATA[";
	const char* end_str = "]]>";

	int i; int cur = 0;
	for(i = 0; i < pos; ++i){
		if(buffer[i] == begin_str[cur]) ++cur;
		else cur = 0;
		if(cur == strlen(begin_str)){
			begin = buffer+i+1; break;
		}
	}

	cur = 0;
	for(i = i+1; i < pos; ++i){
		if(buffer[i] == end_str[cur]) ++cur;
		else cur = 0;
		if(cur == strlen(end_str)){
			end = buffer+i-strlen(end_str); break;
		}
	}

	for(; begin <= end; ++begin) fputc(*begin, fileout);
	fputc(0x0A, fileout);
	free(buffer);
	fclose(fileout);

}
